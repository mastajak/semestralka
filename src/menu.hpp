#include <string>
#include <Arduino.h>
class Menu
{
    String Text;    //Obsah paketu
    int packetID;   //Kod paketu
    public:
    int Value;      //hodnota
    Menu(int ID, String text, int val): packetID(ID), Text(text), Value(val){};
    void New(int newVal)
    {
        this->Value = newVal;
    }
    void draw(int line);
    void refresh(int line);
};