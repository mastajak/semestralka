#include "roomba.hpp"
#include <Arduino.h>
#include <SPI.h>
#include <Wire.h>
#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>
#include <vector>

#define del 50
#define Start 128 //makra příkazů 
#define Stop 173
#define Safe 131
#define Full 132
#define Clean 135
#define Spot 134
#define Home 143
#define Sensors 142
#define PowerOff 133



HardwareSerial roomba(2); //sériová linka pro komunikaci s robotem

Roomba::Roomba(int rxPin, int txPin) //inicializace
     {
        roomba.begin(115200, SERIAL_8N1, rxPin, txPin);
    }

void Roomba::Read(Input& vystup, int PacketID) //čtení příchozí komunikace
{   
    std::vector<byte> vstup;
    delay(150);
    while(roomba.available())
    {
        byte c = roomba.read();
        vstup.push_back(c);
    }
    vystup.read(PacketID, vstup);
}

void Roomba::Request(int PacketID) //Enters safe mode, clears potencial streams, request packet
{

    while (roomba.available() > 0) { //vymazat buffer
        char c = roomba.read();
    } 
    delay(del);
    roomba.write(150); // Stop potencial streams
    delay(del);
    roomba.write(0);   //stop
    delay(del);
    roomba.write(Sensors);
    delay(150);
    roomba.write(PacketID);
}