#include "menu.hpp"
#include "roomba.hpp"
#include <Arduino.h>
#include <SPI.h>
#include <Wire.h>
#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>
#include <vector>
#include <string>

extern Adafruit_SSD1306 display;
extern Roomba robot; 
extern String packet_names[];
void Menu::draw(int line) //načte a vykreslí
{
    Input input(packetID, 0);
    robot.Request(packetID);
    delay(5);
    robot.Read(input, packetID);
    Value = input.val();
    //Text = packet_names[input.typ()-7];

    delay(10);
    // Serial.println(Text);
    display.setCursor(0,line*8);
    display.fillRect(0, 8*line, 128, 8, BLACK);
    display.print(Text);
    display.setCursor(128-5*6, line*8);
    display.print(Value);
    display.display();
}
void Menu::refresh(int line) //načte a obnoví hodnoty
{
    Input input(packetID, 0);
    robot.Request(packetID);
    delay(5);
    robot.Read(input, packetID);
    Value = input.val();
    delay(10);
    display.fillRect(128-5*6, 8*line, 128, 8, BLACK);
    display.setCursor(128-5*6, line*8);
    display.print(Value);
}

