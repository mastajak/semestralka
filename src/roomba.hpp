#include <Arduino.h>
#include <SPI.h>
#include <Wire.h>
#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>
#include <vector>

#define RX1 16
#define TX1 17


class Input         //pomocná třída čtení z robota
{
    int PacketType;
    int Value;

    public:
        Input(int typ, int value): PacketType(typ), Value(value){};
        void read(int typ, std::vector<byte> bytes)//skládá byty na int TODO, signed unsigned
        {
            this->PacketType = typ;
            int value = 0; 
            for (int i = 0; i < bytes.size(); i++)
            {
                value = value << 8;
                value |= bytes[i];
            }
            if (value > 32768) //likely signed
            {
                value = (value - 32768)*(-1);
            }
            
            this->Value = value; 
        }
      
        int typ(){return this->PacketType;}
        int val(){return this->Value;}

};
class Roomba   //Robot
{
    public:
        Roomba(int rxPin, int txPin);
        void Read(Input& vystup, int PacketID);
        void Send(int kod, int val, int byteCnt);
        void Request(int PacketID);

};
