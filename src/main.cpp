#include <Arduino.h>
#include <SPI.h>
#include <Wire.h>
#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>
#include <WiFi.h>
#include <WiFiClient.h>
#include <WiFiAP.h>
#include "menu.hpp"
#include "roomba.hpp"

#define WIRE Wire

#define RX1 16
#define TX1 17

#define Start 128 //makra příkazů 
#define Stop 173
#define Safe 131
#define Full 132
#define Clean 135
#define Spot 134
#define Home 143
#define PowerOff 133

#define buttonL 26 //tlačítka
#define buttonR 19
#define SCREEN_WIDTH 128
#define SCREEN_HEIGHT 64

Adafruit_SSD1306 display(SCREEN_WIDTH, SCREEN_HEIGHT, &Wire, -1);
Roomba robot(16, 17); 
extern HardwareSerial roomba;

const char* ssid = "TP-Link_7BCE";  //nastavení wifi
const char* password = "18499520";
IPAddress local_IP(192, 168, 0, 129);  
IPAddress gateway(192, 168, 0, 1);     
IPAddress subnet(255, 255, 255, 0);
const char* host = "192.168.0.107";  
const uint16_t port = 1234;
WiFiServer server(1234); // Port number

String packet_names[] = {     //Mapa názvů packetů -7
    "Bumps Wheeldrops",
    "Wall",
    "Cliff Left",
    "Cliff Front Left",
    "Cliff Front Right",
    "Cliff Right",
    "Virtual Wall",
    "Overcurrents",
    "Dirt Detect",
    "Unused 1",
    "Ir Opcode",
    "Buttons",
    "Distance",
    "Angle",
    "Charging State",
    "Voltage",
    "Current",
    "Temperature",
    "Battery Charge",
    "Battery Capacity",
    "Wall Signal",
    "Cliff Left Signal",
    "Cliff Front Left Signal",
    "Cliff Front Right Signal",
    "Cliff Right Signal",
    "Unused 2",
    "Unused 3",
    "Charger Available",
    "Open Interface Mode",
    "Song Number",
    "Song Playing?",
    "Oi Stream Num Packets",
    "Velocity",
    "Radius",
    "Velocity Right",
    "Velocity Left",
    "Encoder Counts Left",
    "Encoder Counts Right",
    "Light Bumper",
    "Light Bump Left",
    "Light Bump Front Left",
    "Light Bump Center Left",
    "Light Bump Center Right",
    "Light Bump Front Right",
    "Light Bump Right",
    "Ir Opcode Left",
    "Ir Opcode Right",
    "Left Motor Current",
    "Right Motor Current",
    "Main Brush Current",
    "Side Brush Current",
    "Stasis"
  };




int driveDirect(int speedL, int speedR)   //funkce pro pohyb
{
 roomba.write(146); //Drive direct, speed in mm/s
  roomba.write(speedL>>8); //Speed L high byte
  roomba.write(speedL); //low byte
  roomba.write(speedR>>8); //speed R
  roomba.write(speedR);
  return 0;
}

void setPowerLED(byte setColor, byte setIntensity) //funkce pro nastavení barvy led
{
    int color = setColor;
    int intensity = setIntensity;
    roomba.write(139);
    delay(10);
    roomba.write((byte)0x00);
    delay(10);
    roomba.write((byte)color);
    delay(10);
    roomba.write((byte)intensity);
    delay(10);
}

void displayinit()  //inicializace displeje
{
  if (!display.begin(SSD1306_SWITCHCAPVCC, 0x3C)) {  
      Serial.println(F("SSD1306 allocation failed"));
      for (;;) ;
    }
    display.clearDisplay();
    display.setTextSize(1);
    display.setTextColor(SSD1306_WHITE);
    display.setCursor(0,0);
    display.display();

}

void wifiinit()   //inicializace wifi
{
  if (!WiFi.config(local_IP, gateway, subnet)) {
    Serial.println("STA Failed to configure");
  }
  WiFi.begin(ssid, password);
  while (WiFi.status() != WL_CONNECTED) {
    delay(1000);
    Serial.println("Connecting to WiFi...");
  }
  Serial.println("Connected to WiFi");
  server.begin();
  Serial.println("Server started");

  Serial.print("IP address: ");
  Serial.println(WiFi.localIP());
}

int expectint(WiFiClient& client) //čtení z QT
{
  while(!client.available()){
    delay(1);
  }

  // čtení dotazu
  int receivedInteger;
        client.read(reinterpret_cast<uint8_t*>(&receivedInteger), sizeof(receivedInteger));
        Serial.print("Received integer: ");
        Serial.println(receivedInteger);
  return receivedInteger;
}

// časovač přerušení
hw_timer_t *timer = NULL;
volatile bool toggle = false;

// Interrupt service routine (ISR)
void IRAM_ATTR onTimer() {
  toggle = !toggle;  // flag pro obnovení displeje
}

void setup() 
{
    Serial.begin(9600);   //Nastavení rychlosti sériové linky pro pc                         
    roomba.begin(115200, SERIAL_8N1, RX1, TX1); //nastavení hardwarové sériové linky pro roombu115200
    delay(1000); //sanity check
    displayinit();
    roomba.write(Start); //probuzení roomby
    delay(500);
    wifiinit();

      // Nastavení ISR
      timer = timerBegin(0, 80, true);  // Timer 0, prescaler 80 (1 MHz), count up
      timerAttachInterrupt(timer, &onTimer, true);  // Attach ISR
      timerAlarmWrite(timer, 300000, true);  // Set timer to 1 second (1,000,000 us)
      timerAlarmEnable(timer);  // Enable the timer interrupt
  setPowerLED (128, 128); //nastavení led na oranžovou barvu
}

int greatsucces = 0;
std::vector<Menu> mainmenu; //Menu 

void loop() {
  WiFiClient client = server.available();
    if (toggle&&mainmenu.size()){ //refresh displeje
        if (mainmenu.size()>8)  //vymazání když je plný
        {
          mainmenu.clear();
          display.clearDisplay();
          display.display();
        }
      for (int i = 0; i < mainmenu.size(); i++) //refresh
    {
      mainmenu[i].refresh(i);
      delay(5);
    }
    display.display();
    toggle = 0;
  } 
  
  if (!client) {  
    return;
  }
  //čekání na integer
  int cislo = expectint(client);

  if (cislo == 741) //kod pro požadavek
  {
    Serial.println("Input");
    greatsucces = 1;
    return;
  }
  if (cislo == 963) //reset menu
  {
          mainmenu.clear();
          display.clearDisplay();
          display.display();
  }
  else
  {
    if (!greatsucces)
    {
      roomba.write(cislo); //přeposlání příkazu do roomby
      Serial.println("Sent!");
    }
  }

  if (greatsucces == 1) //request packetu z roomby, menu
  {
      Input input(cislo, 0);
      Menu tmp(cislo, packet_names[cislo-7], 0);
      mainmenu.push_back(tmp);
      greatsucces = 0;
    for (int i = 0; i < mainmenu.size(); i++) //plné vykreslení menu
    {
      mainmenu[i].draw(i);
      delay(5);
    }
    display.display();

  }
  Serial.println(packet_names[cislo-7]);
}