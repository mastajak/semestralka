# Ovládání vysavače Roomba pomocí ESP32

Tento projekt je součástí semestrální práce zaměřené na ovládání vysavače Roomba pomocí ESP32 mikrokontroléru a komunikaci s počítačem prostřednictvím Wi-Fi s využitím frameworku Qt.

## Popis projektu
Semestrální práce se zaměřuje na komunikaci mezi počítačem a vysavačem roomba. Ta je řešena pomocí ESP32, to umožnuje posílání příkazů, vyžádání informací a jejich zobrazení na displeji. Robot je tak tedy možné na dálku přes wifi ovládat. Grafické prostředí desktopové aplikace je řešeno pomocí Qt frameworku, grafické prostředí obsahuje základní ovládácí prvky, zároveň umožnuje robotu vysílat příkazy.
Informace o komunikaci s robotem jako je obsah jednotlivých packetů a podobně najdete v iRobot_Roomba_600_Open_Interface_Spec.pdf

## Funkce projektu

- **Dálkové ovládání:** Umožnuje vzdálené ovládání vysavače
- **Stavové zprávy:** Zobrazuje aktuální stav vysavače, nabití baterie, stav senzorů a podobně přímo na vysavači.
- **Displej vysavače:** Pomocí grafického prostředí je možné vyžádat až osm parametrů které se budou přímo na displeji zobrazovat. Grafické prostředí rovněž umožnuje menu vymazat.
- **Uživatelské rozhraní:** Qt framework poskytuje interaktivní a přehledné rozhraní pro uživatele, které zahrnuje ovládací prvky pro ovládání vysavače.
### Video Demo
Videa a fotografie jsou ve složce photos

![Video Demo](giphy.gif){: width="320px" height="180px"}

# Fotogalerie

#### Obrázek 1
<img src="Photos/IMG1.jpg" alt="Obrázek 1" width="400" />

#### Obrázek 2
<img src="Photos/IMG2.jpg" alt="Obrázek 2" width="400" />

#### Obrázek 3 UI
<img src="Photos/IMG3.jpg" alt="Obrázek 3" width="400" />


## Instalace a použití

1. **Nainstalujte Qt a platformio:** Pro vývoj a spuštění aplikace je nezbytné mít nainstalovaný Qt framework a platformio. Instalační pokyny lze nalézt na [oficiální stránce Qt](https://www.qt.io/download).
   
2. **Klonování repozitáře:**
   ```bash
   git clone https://gitlab.fel.cvut.cz/mastajak/semestralka.git
