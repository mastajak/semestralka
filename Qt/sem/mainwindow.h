#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QTcpSocket>

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private slots:
    void on_pushButton_clicked();
    void on_pushButton2_clicked();
    void on_pushButtonClean_clicked();
    void on_pushButtonSpot_clicked();
    void on_pushButtonHome_clicked();
    void on_pushButtonSafe_clicked();
    void on_pushButtonFull_clicked();
    void on_pushButtonPasive_clicked();
    void on_pushButtonStart_clicked();
    void on_pushButtonReset_clicked();
    void on_pushButtonStop_clicked();
    void sendint(int integer);


private:
    Ui::MainWindow *ui;
    QTcpSocket *socket;
};
#endif // MAINWINDOW_H
