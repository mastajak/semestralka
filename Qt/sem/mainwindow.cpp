#include "mainwindow.h"
#include "build/Desktop_Qt_6_7_2_MinGW_64_bit-Debug/ui_mainwindow.h"
#include "ui_mainwindow.h"
#include <QDebug>



MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
    , socket(new QTcpSocket(this))
{
    ui->setupUi(this);
    this->setFixedSize(this->size());
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::sendint(int Integer)
{
    int integer = Integer;
    socket->connectToHost("192.168.0.129", 1234);

    if (socket->waitForConnected(5000)) {
        qDebug() << "Connected to ESP32!";

        QByteArray data;
        data.append(reinterpret_cast<const char*>(&integer), sizeof(integer));
        socket->write(data);
        socket->waitForBytesWritten();
        socket->disconnectFromHost();
    } else {
        qDebug() << "Failed to connect to ESP32:" << socket->errorString();
    }
}






void MainWindow::on_pushButton_clicked()
{
    bool ok;
    int integerToSend = ui->lineEdit->text().toInt(&ok);

    sendint(integerToSend);
}
void MainWindow::on_pushButton2_clicked()
{
    bool ok;
    int integerToSend = ui->lineEdit2->text().toInt(&ok);
    sendint(741);
    sendint(integerToSend);
}

void MainWindow::on_pushButtonClean_clicked()
{
    sendint(135);
}
void MainWindow::on_pushButtonSpot_clicked()
{
    sendint(134);
}
void MainWindow::on_pushButtonHome_clicked()
{
    sendint(143);
}
void MainWindow::on_pushButtonSafe_clicked()
{
    sendint(131);
}
void MainWindow::on_pushButtonFull_clicked()
{
    sendint(132);
}
void MainWindow::on_pushButtonPasive_clicked()
{
    sendint(133);
}
void MainWindow::on_pushButtonStart_clicked()
{
    sendint(128);
}
void MainWindow::on_pushButtonReset_clicked()
{
    sendint(7);
}
void MainWindow::on_pushButtonStop_clicked()
{
    sendint(173);
}


