/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 6.7.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QFormLayout>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QWidget *centralwidget;
    QWidget *formLayoutWidget;
    QFormLayout *formLayout;
    QLineEdit *lineEdit;
    QPushButton *pushButton;
    QPushButton *pushButton2;
    QLineEdit *lineEdit2;
    QPushButton *pushButtonClean;
    QPushButton *pushButtonSpot;
    QPushButton *pushButtonHome;
    QPushButton *pushButtonStop;
    QPushButton *pushButtonStart;
    QPushButton *pushButtonReset;
    QPushButton *pushButtonSafe;
    QPushButton *pushButtonFull;
    QPushButton *pushButtonPassive;
    QMenuBar *menubar;
    QStatusBar *statusbar;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName("MainWindow");
        MainWindow->resize(215, 252);
        centralwidget = new QWidget(MainWindow);
        centralwidget->setObjectName("centralwidget");
        formLayoutWidget = new QWidget(centralwidget);
        formLayoutWidget->setObjectName("formLayoutWidget");
        formLayoutWidget->setGeometry(QRect(-1, 9, 211, 191));
        formLayout = new QFormLayout(formLayoutWidget);
        formLayout->setObjectName("formLayout");
        formLayout->setContentsMargins(0, 0, 0, 0);
        lineEdit = new QLineEdit(formLayoutWidget);
        lineEdit->setObjectName("lineEdit");

        formLayout->setWidget(0, QFormLayout::FieldRole, lineEdit);

        pushButton = new QPushButton(formLayoutWidget);
        pushButton->setObjectName("pushButton");

        formLayout->setWidget(0, QFormLayout::LabelRole, pushButton);

        pushButton2 = new QPushButton(formLayoutWidget);
        pushButton2->setObjectName("pushButton2");

        formLayout->setWidget(1, QFormLayout::LabelRole, pushButton2);

        lineEdit2 = new QLineEdit(formLayoutWidget);
        lineEdit2->setObjectName("lineEdit2");

        formLayout->setWidget(1, QFormLayout::FieldRole, lineEdit2);

        pushButtonClean = new QPushButton(formLayoutWidget);
        pushButtonClean->setObjectName("pushButtonClean");

        formLayout->setWidget(2, QFormLayout::SpanningRole, pushButtonClean);

        pushButtonSpot = new QPushButton(formLayoutWidget);
        pushButtonSpot->setObjectName("pushButtonSpot");

        formLayout->setWidget(3, QFormLayout::LabelRole, pushButtonSpot);

        pushButtonHome = new QPushButton(formLayoutWidget);
        pushButtonHome->setObjectName("pushButtonHome");

        formLayout->setWidget(3, QFormLayout::FieldRole, pushButtonHome);

        pushButtonStop = new QPushButton(formLayoutWidget);
        pushButtonStop->setObjectName("pushButtonStop");

        formLayout->setWidget(7, QFormLayout::FieldRole, pushButtonStop);

        pushButtonStart = new QPushButton(formLayoutWidget);
        pushButtonStart->setObjectName("pushButtonStart");

        formLayout->setWidget(5, QFormLayout::FieldRole, pushButtonStart);

        pushButtonReset = new QPushButton(formLayoutWidget);
        pushButtonReset->setObjectName("pushButtonReset");

        formLayout->setWidget(6, QFormLayout::FieldRole, pushButtonReset);

        pushButtonSafe = new QPushButton(formLayoutWidget);
        pushButtonSafe->setObjectName("pushButtonSafe");

        formLayout->setWidget(5, QFormLayout::LabelRole, pushButtonSafe);

        pushButtonFull = new QPushButton(formLayoutWidget);
        pushButtonFull->setObjectName("pushButtonFull");

        formLayout->setWidget(6, QFormLayout::LabelRole, pushButtonFull);

        pushButtonPassive = new QPushButton(formLayoutWidget);
        pushButtonPassive->setObjectName("pushButtonPassive");

        formLayout->setWidget(7, QFormLayout::LabelRole, pushButtonPassive);

        MainWindow->setCentralWidget(centralwidget);
        menubar = new QMenuBar(MainWindow);
        menubar->setObjectName("menubar");
        menubar->setGeometry(QRect(0, 0, 215, 22));
        MainWindow->setMenuBar(menubar);
        statusbar = new QStatusBar(MainWindow);
        statusbar->setObjectName("statusbar");
        MainWindow->setStatusBar(statusbar);

        retranslateUi(MainWindow);

        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QCoreApplication::translate("MainWindow", "MainWindow", nullptr));
        pushButton->setText(QCoreApplication::translate("MainWindow", "Send packet", nullptr));
        pushButton2->setText(QCoreApplication::translate("MainWindow", "Request", nullptr));
        pushButtonClean->setText(QCoreApplication::translate("MainWindow", "Clean", nullptr));
        pushButtonSpot->setText(QCoreApplication::translate("MainWindow", "Spot", nullptr));
        pushButtonHome->setText(QCoreApplication::translate("MainWindow", "Home", nullptr));
        pushButtonStop->setText(QCoreApplication::translate("MainWindow", "Stop", nullptr));
        pushButtonStart->setText(QCoreApplication::translate("MainWindow", "Start comunication", nullptr));
        pushButtonReset->setText(QCoreApplication::translate("MainWindow", "Reset roomba", nullptr));
        pushButtonSafe->setText(QCoreApplication::translate("MainWindow", "Safe", nullptr));
        pushButtonFull->setText(QCoreApplication::translate("MainWindow", "Full", nullptr));
        pushButtonPassive->setText(QCoreApplication::translate("MainWindow", "Passive", nullptr));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
