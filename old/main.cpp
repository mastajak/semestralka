// main.cpp

#include <QCoreApplication>
#include <QTcpSocket>
#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QDebug>

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    QTcpSocket socket;
    socket.connectToHost("192.168.0.104", 1234); // Replace with your ESP32 IP and port

    if(socket.waitForConnected(3000))
    {
        qDebug() << "Connected to ESP32!";
        int integerToSend = 42;

        QByteArray data;
        data.append(reinterpret_cast<const char*>(&integerToSend), sizeof(integerToSend));

        socket.write(data);
        socket.waitForBytesWritten();
        socket.disconnectFromHost();

    }
    else
    {
        qDebug() << "Failed to connect to ESP32:" << socket.errorString();
    }

    return a.exec();
}
